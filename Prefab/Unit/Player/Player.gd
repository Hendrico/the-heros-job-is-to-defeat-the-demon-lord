extends "res://Prefab/Unit/Unit.gd"

 
func start():
	GameManager.players.append(self)
	type = "Player"

func death():
	GameManager.queueTurn.erase(self)
	GameManager.players.erase(self)
	self.visible = false
	
func levelUp(name, nextLevel):
	for i in range(len(skills)):
		if (skills[i].skillName == name):
			var newSkill = nextLevel.instance()
			newSkill.start()
			newSkill.currentCd = newSkill.cd
			skills[i] = newSkill

func startTurn():
	UiManager.uiDict["SkillGroup"].setSkill(skills)
	for i in skills:
		i.setValue(self)
	.startTurn()
	

func endTurn():
	UiManager.uiDict["SkillGroup"].resetSkill()
	.endTurn()
	
