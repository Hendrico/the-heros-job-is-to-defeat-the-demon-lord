extends "res://Prefab/Unit/Player/Player.gd"

var firstLow = true

func takeDamage(damage):
	.takeDamage(damage)
	if (firstLow and health < maxHealth * 0.25):
		firstLow = false
		GameManager.pause()
		GameManager.singleton["LevelManager"].addKnight()
		

func death():
	if (firstLow):
		firstLow = false
		health = maxHealth * 0.25
		emit_signal("onHealthChange", health)
		GameManager.pause()
		GameManager.singleton["LevelManager"].addKnight()
		
	else:
		.death()
		
