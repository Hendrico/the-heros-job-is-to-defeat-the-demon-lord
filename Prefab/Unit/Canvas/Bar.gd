extends Control

func _ready():
	pass # Replace with function body.


func onHealthChange(health):
	$HealthBar.value = health
	$Bg/HpText.text = str(health)



func setMaxHealth(health):
	$HealthBar.max_value = health
	$HealthBar.value = health
	$Bg/HpText.text = str(health)

func onAttackBarIncrease(attackBar):
	$AttackBar.value = attackBar
