extends Node

export(String) var unitName
export(int) var maxHealth
var health
export(int) var baseAttack
var attack
export(int) var baseDefense
var defense
export(float) var baseSpeed
var speed
var stun
onready var buffManager = get_node("BuffManager")
var type
var attackBar = 0
export(Array) var skills 
var stGenerator
signal onHealthChange(health)
signal setMaxHealth(health)
signal onAttackBarIncrease(attackBar)

func _ready():
	connect("onHealthChange", get_node("Bar"), "onHealthChange")
	connect("setMaxHealth", get_node("Bar"), "setMaxHealth")
	connect("onAttackBarIncrease", get_node("Bar"), "onAttackBarIncrease")
	get_node("Area2D").connect("input_event", self, "onArea2DInputEvent")
	health = maxHealth
	attack = baseAttack
	defense = baseDefense
	speed = baseSpeed
	stGenerator = $STGenerator
	for i in range (len(skills)):
		skills[i] = get_node(skills[i])
	emit_signal("setMaxHealth", health)

func start():
	pass

func takeDamage(damage):
	damage -= defense * 0.25
	$Sprite.modulate = Color(1, 0, 0) # red shade
	$Audio/Hit.play()
	if (damage < 0):
		damage = 0
	else:
		damage = int(ceil(damage))
	health -= damage
	stGenerator.createText(str(damage))
	if (health <= 0):
		health = 0
		death()
	emit_signal("onHealthChange", health)
	yield(get_tree().create_timer(0.3), "timeout")
	$Sprite.modulate = Color(1, 1, 1) # reset to default

func updateSpeed():
	attackBar += speed
	emit_signal("onAttackBarIncrease", attackBar)
	if (attackBar >= 100):
		if (buffManager.buffDictionary["Stun"][0] >= 1):
			resetAttackBar()
			endTurn()
		else:	
			GameManager.queueTurn.append(self)

func increaseAttackBar(booster):
	attackBar += booster
	emit_signal("onAttackBarIncrease", attackBar)
	if (attackBar < 0):
		attackBar = 0
	if (attackBar >= 100):
		if (buffManager.buffDictionary["Stun"][0] >= 1):
			resetAttackBar()
			endTurn()
		else:	
			GameManager.queueTurn.append(self)

func healing(heal):
	$Sprite.modulate = Color(0, 1, 0) # green shade
	if (buffManager.buffDictionary["Wound"][0] >= 1):
		heal /= 2
	heal = int(round((heal * maxHealth)/100))
	health += heal
	$Audio/Heal.play()
	stGenerator.createText(str(heal))
	if (health > maxHealth):
		health = maxHealth
	emit_signal("onHealthChange", health)
	yield(get_tree().create_timer(0.3), "timeout")
	$Sprite.modulate = Color(1, 1, 1) # reset to default

func setTarget(isSet):
	if (isSet):
		$Sprite.modulate = Color(1, 1, 0) 
	else:
		$Sprite.modulate = Color(1, 1, 1) # reset to default

func startTurn():
	resetAttackBar()
	if (buffManager.buffDictionary["Stun"][0] >= 1):
			endTurn()	
	
func endTurn():
	buffManager.decreaseBuffTime()
	decreaseSkillCooldown()
	
func resetAttackBar():
	attackBar = 0
	emit_signal("onAttackBarIncrease", attackBar)

func onArea2DInputEvent(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.is_pressed() &&  is_in_group(GameManager.targetType):
			GameManager.onTargetChoosen(self)

func death():
	pass

func levelUp(name, nextLevel):
	pass

func revive():
	healing(100)
	resetAttackBar()
	self.visible = true
	buffManager.activateBuff("Cleanse", 0)
	buffManager.resetBuff()

func decreaseSkillCooldown():
	for i in skills:
		i.reduceCooldown()
