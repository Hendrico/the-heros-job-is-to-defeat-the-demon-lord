extends "res://Prefab/Unit/Unit.gd"

var turn = 0
export(int) var maxTurn = 0

func start():
	GameManager.enemies.append(self)
	type = "Enemy"

func death():
	GameManager.queueTurn.erase(self)
	GameManager.enemies.erase(self)
	queue_free()

func startTurn():
	for i in skills:
		i.setValue(self)
	.startTurn()
	chooseSkill(turn % maxTurn)
	chooseEnemy()

func endTurn():
	turn+=1
	.endTurn()
	

func chooseSkill(turn):
	GameManager.setSkill(skills[turn])
	
func chooseEnemy():
	GameManager.setDefender(GameManager.players[randi() % GameManager.players.size()])
