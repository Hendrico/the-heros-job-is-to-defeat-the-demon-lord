extends Node

onready var unit = get_parent()
export(Dictionary) var buffDictionary = {
	"Attack Break" : [0],
	"Defense Break" : [0],
	"Slow" : [0],
	"Attack Boost": [0],
	"Defense Boost" : [0],
	"Speed Up" : [0],
	"Stun" : [0],
	"Wound" : [0],
	}
var deathCountdown = 0

func activateBuff(buff, value):
	if (buff == "Attack Break"):
		unit.attack -= unit.baseAttack * 0.5
	elif (buff == "Defense Break"):
		unit.defense -= unit.baseDefense * 0.5
	elif (buff ==  "Slow"):
		unit.speed -= unit.baseSpeed * 0.25	
	elif (buff == "PushBack"):
		unit.increaseAttackBar(-value) 
	elif (buff == "Attack Boost"):
		unit.attack += unit.baseAttack * 0.5
	elif (buff == "Defense Boost"):
		unit.defense += unit.baseDefense * 0.5
	elif (buff == "Speed Up"):
		unit.speed += unit.baseSpeed * 0.25	
	elif (buff == "Booster"):
		unit.increaseAttackBar(value)
	elif (buff == "Cleanse"):
		for i in buffDictionary:
			if buffDictionary[i][2] != null:
				buffDictionary[i][2].queue_free()
				buffDictionary[i][2] = null
			buffDictionary[i][0] = 0
		resetBuff()
	elif (buff == "Attack"):
		randomize()
		var critChance = rand_range(0, 100)
		if (critChance < 15):
			value *= 1.5
			unit.stGenerator.createText("Crit!")
			yield(get_tree().create_timer(0.3), "timeout")
		value = rand_range(value * 0.75, value * 1.25)
		unit.takeDamage(value)
	elif (buff == "Healing"):
		value = rand_range(value * 0.75, value * 1.25)
		unit.healing(value)
	elif (buff == "Death"):
		deathCountdown = 5
	
		
func addBuff(buff):
	if (not buff.isDirect):
		# Resistance check
		randomize()
		var chance = rand_range(0, 1)
		if (chance > buff.chance):
			$Miss.play()
			unit.stGenerator.createText("Miss!")
			return

		if buffDictionary[buff.buffName][0] == 0: # Buff not active
			buffDictionary[buff.buffName][0] = buff.value
			instantiateBuffIcon(buff)
			activateBuff(buff.buffName, 0)
		elif buffDictionary[buff.buffName][0] < buff.value: # Buff already active
			changeIconTime(buff.buffName, buff.value)
			buffDictionary[buff.buffName][0] = buff.value
	else: 
		if (buff.buffName != "Attack" and buff.buffName != "Healing"):
			randomize()
			var chance = rand_range(0, 1)
			if (chance > buff.chance):
				$Miss.play()
				unit.stGenerator.createText("Miss!")
				return
		activateBuff(buff.buffName, buff.value)

func instantiateBuffIcon(buff):
	var buffObj = buffDictionary[buff.buffName][1].instance()
	buffObj.get_node("TurnRemaining").text = str(buff.value)
	get_node("BuffContainer").add_child(buffObj)
	buffDictionary[buff.buffName][2] = buffObj

func changeIconTime(name, time):
	buffDictionary[name][2].get_node("TurnRemaining").text = str(time)

func decreaseBuffTime():
	resetBuff()
	for buff in buffDictionary:
		var temp = buffDictionary[buff][0] - 1
		if (temp <= 0):
			if buffDictionary[buff][2] != null:
				buffDictionary[buff][2].queue_free()
				buffDictionary[buff][2] = null
			temp = 0
		else:
			changeIconTime(buff, temp)
			activateBuff(buff, 0)
		buffDictionary[buff][0] = temp
	
	# For death debuff
	if (deathCountdown != 0):
		deathCountdown -= 0
		if (deathCountdown == 0):
			unit.takeDamage(999999)

func resetBuff():
	unit.attack = unit.baseAttack
	unit.defense = unit.baseDefense
	unit.speed = unit.baseSpeed
