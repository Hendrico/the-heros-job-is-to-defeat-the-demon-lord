extends Node

export(String) var buffName
export(int) var value
export(bool) var isAoe
export(bool) var forAlly
enum ScalingType {HP, ATK, DEF, NONE}
export(ScalingType) var type
export(bool) var isDirect
export(float) var multiplier
export(float) var chance

func setValue(unit):
	if (type == ScalingType.NONE):
		return
	elif (type == ScalingType.HP):
		value = unit.maxHealth * multiplier
	elif (type == ScalingType.DEF):
		value = unit.defense * multiplier
	elif (type == ScalingType.ATK):
		value = unit.attack * multiplier
	randomize()
	value = rand_range(value * 0.75, value * 1.25)
	


