extends "res://Prefab/Manager/BattleState/IBattleState.gd"

func doState():
	if (GameManager.skill != null and GameManager.defenders != null and not isRun):
		isRun = true
		SkillManager.activateSkill()
		

func onSkillClicked(skill):
	if (not isRun):
		GameManager.setSkill(skill)
		GameManager.targetType = skill.targetType
		GameManager.setTargetSelector()
	
func onTargetChoosen(target):
	if (GameManager.skill != null and not isRun):
		GameManager.setDefender(target)
		GameManager.resetTargetSelector()

func endTurn():
	GameManager.reset()
	isRun = false
	GameManager.state = load(updateSpeedPath).new()
