extends "res://Prefab/Manager/BattleState/IBattleState.gd"


func doState():
	if (not isRun):
		isRun = true
		GameManager.attackers.startTurn()
		SkillManager.activateSkill()

	
func endTurn():
	GameManager.reset()
	isRun = false
	GameManager.state = load(updateSpeedPath).new()
