extends "res://Prefab/Manager/BattleState/IBattleState.gd"


func doState():
	
	if (Tools.countNotNull(GameManager.players) == 0):
		GameManager.isRun = false
		GameManager.pause()
		if (GameManager.status["playerDeath"] == 0):
			GameManager.status["playerDeath"] += 1
			GameManager.singleton["LevelManager"].addPriest()
		else:
			GameManager.singleton["LevelManager"].lose()
	elif (Tools.countNotNull(GameManager.enemies) == 0):
		GameManager.isRun = false
		if (GameManager.status["enemiesDeath"] == 0):
			GameManager.status["enemiesDeath"] += 1
			GameManager.singleton["LevelManager"].addPhase2()
		else:
			GameManager.singleton["LevelManager"].win()
	elif (GameManager.queueTurn.empty() && GameManager.isRun):
		for player in GameManager.players:
			player.updateSpeed()
		for enemy in GameManager.enemies:
			enemy.updateSpeed() 
	else:
		GameManager.queueTurn.sort_custom(QueueTurnSorter, "sort_ascending")
		GameManager.attackers = GameManager.queueTurn.pop_back()
		GameManager.attackers.startTurn()
		if GameManager.attackers.is_in_group("Player"):
			GameManager.state = load(playerTurnPath).new()
		else:
			GameManager.state = load(enemyTurnPath).new()

class QueueTurnSorter:
	static func sort_ascending(a, b):
		if a.attackBar < b.attackBar:
			return true
		return false
