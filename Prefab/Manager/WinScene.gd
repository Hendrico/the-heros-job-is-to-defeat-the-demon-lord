extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var isFinish = false
# Called when the node enters the scene tree for the first time.
func _ready():
	$GameOver.hide()
	$GameOver.set_process(false)
	$BgText/Label/AnimatedSprite.play("RollingText")
	yield($BgText/Label/AnimatedSprite, "animation_finished")
	$GameOver.show()
	$GameOver.set_process(true)
	
func _physics_process(delta):
	if Input.is_action_pressed("fire") && not isFinish:
		Engine.time_scale = 2
	else:
		Engine.time_scale = 1
