extends Node2D


export(Dictionary) var players
export(Dictionary) var enemies
var dialog = []
signal dialogFinish()
# Called when the node enters the scene tree for the first time.
func _init():
	GameManager.singleton["LevelManager"] = self

func _ready():
	for player in players:
		players[player] = get_node(players[player])
		players[player].hide()
	for enemy in enemies:
		enemies[enemy] = get_node(enemies[enemy])
		enemies[enemy].hide()
	summon("hero", "player")
	summon("phase1", "enemy")
	startDialog([
		["Hero", "Drol Nomed! Your time is up!"],
		["Drol Nomed", "Oh? You're approaching me? Instead of running away, you're coming right to me?"],
		["Hero", "I can't beat the shit out of you without getting closer."],
		["Drol Nomed", "Oh ho! Then come as close as you like."]
	])
	yield(self, "dialogFinish")
	GameManager.isRun = true
	GameManager.start()

func startDialog(dialog):
	UiManager.uiDict["SkillGroup"].hide()
	self.dialog = dialog
	var text = dialog.pop_front()
	GameManager.singleton["Dialog"].setText(text[0], text[1])
	UiManager.uiDict["Dialog"].show()
	

func onDialogClick():
	var text = dialog.pop_front()
	if (text != null):
		GameManager.singleton["Dialog"].setText(text[0], text[1])
	else:
		UiManager.uiDict["Dialog"].hide()
		UiManager.uiDict["SkillGroup"].show()
		emit_signal("dialogFinish")

func addKnight():
	GameManager.pause()
	startDialog([
		[GameManager.enemies[0].unitName, "You can't defeat me."],
		["Hero", "I know, but he can."],
	])
	yield(self, "dialogFinish")
	summon("knight", "player")
	
	startDialog([
		["Narator", "Knight join the party"],
		["Knight", "My god forgive you, but I won't."],
		[GameManager.enemies[0].unitName, "You are courting death!"]
	])
	yield(self, "dialogFinish")
	GameManager.isRun = true
	GameManager.start()

func addPriest():
	GameManager.pause()
	startDialog([
		[GameManager.enemies[0].unitName, "You puny human won't be able to defeat me."]
	])
	yield(self, "dialogFinish")
	summon("priest", "player")
	startDialog([
		["Priest", "Sorry, I'm late. What am I missing?"],
		[GameManager.enemies[0].unitName, "I already beat your friend. Next up is you!"],
		["Priest", "That's not a problem."],
		["Priest", "A R I S E."]
	])
	yield(self, "dialogFinish")
	revive("hero")
	revive("knight")
	startDialog([
		[GameManager.enemies[0].unitName, "Alright, prepare to die for the second time!"]
	])
	yield(self, "dialogFinish")
	GameManager.isRun = true
	GameManager.start()
	
func addPhase2():
	GameManager.pause()
	startDialog([
		["Hero", "It's over Drol Nomed! I have the high ground!"],
		["Drol Nomed", "Do you think it's over? It isn't even my final form"],
		["Narator", "What? \nDrol Nomed is evolving!"]
	])
	yield(self, "dialogFinish")
	summon("phase2", "enemy")
	startDialog([
		["Narator", "Drol Nomed evolved into Drol Nomed Final Form!"],
		["Drol Nomed Final Form", "It's time for the second round."],
		["Hero", "Ah shit, here we go again."]
	])
	yield(self, "dialogFinish")
	GameManager.isRun = true
	GameManager.start()
	
func lose():
	GameManager.pause()
	startDialog([
		[GameManager.enemies[0].unitName, "Even in 1 million years, you won't be able to beat me!"],
		["Hero", "I...can't lose here.."],
		["Narator", "And hero and party fail and get beaten up by demon lord."],
		["Narator", "Maybe in another timeline, the hero will win."],
		["Narator", "I hope."],
	])
	yield(self, "dialogFinish")
	UiManager.uiDict["transitionScene"].changeScene("LoseScene")

func win():
	GameManager.pause()
	startDialog([
		["Hero", "You are dead now!"],
		["Drol Nomed Final Form", "Do you think it's over?."],
		["Hero", "I already beat you in your final form."],
		["Drol Nomed Final Form", "Do you think this is my final form?"],
		["Drol Nomed Final Form", "I will show you what my true final form is!"],
		["Narator", "Even though it's been defeated 2 times, it's still not enough."],
		["Narator", "Will the hero succeed in defeating the demon lord?"],
		["Narator", "On the next episode of The Hero's Job Is To Defeat the Demon Lord Z!"],
	])
	yield(self, "dialogFinish")
	UiManager.uiDict["transitionScene"].changeScene("WinScene")
	
func revive(name):
	players[name].show()
	players[name].revive()
	players[name].start()
	
func summon(name, type):
	if (type == "player"):
		players[name].show()
		players[name].start()
	
	elif (type == "enemy"):
		enemies[name].show()
		enemies[name].start()
	


func _on_Button_pressed():
	UiManager.uiDict["help"].show()
	UiManager.uiDict["help"].set_process(true)
