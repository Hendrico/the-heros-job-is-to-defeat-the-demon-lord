extends Node

var players = []
var enemies = []
var queueTurn = []
var attackers
var skill
var defenders
var isRun
var state 
var targetType = "None"
var singleton = {}
var status = {
	"playerDeath" : 0,
	"enemiesDeath" : 0
}

signal onSkillChange(skill)
signal onTargetChange(target)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	state.doState()
func _ready():
	pause()
	
func start():
	state = load("res://Prefab/Manager/BattleState/UpdateSpeed.gd").new()
func pause():
	skill = null
	defenders = null
	state = load("res://Prefab/Manager/BattleState/IBattleState.gd").new()

func setSkill(skill):
	self.skill = skill
	UiManager.uiDict["log"].addSkill()

func setDefender(target):
	self.defenders = target
	UiManager.uiDict["log"].addDefender()

func setTargetSelector():
	resetTargetSelector()
	if (targetType == "Player"):
		for i in players:
			i.setTarget(true)
	else:
		for i in enemies:
			i.setTarget(true)

func resetTargetSelector():
	for i in players:
		i.setTarget(false)
	for i in enemies:
		i.setTarget(false)

func reset():
	skill = null
	defenders = null
	UiManager.uiDict["log"].reset()

func onSkillClicked(skill):
	state.onSkillClicked(skill)
	
func onTargetChoosen(target):
	state.onTargetChoosen(target)
	


