extends Control

func setText(name, context):
	$Name.text = name
	$Dialog.text= context

signal OnDialogClick()
func _init():
	GameManager.singleton["Dialog"] = self
	UiManager.uiDict["Dialog"] = self
	
func _ready():
	connect("OnDialogClick", GameManager.singleton["LevelManager"], "onDialogClick")

func _on_Dialog_gui_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			emit_signal("OnDialogClick")
