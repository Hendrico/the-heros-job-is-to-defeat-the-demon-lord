extends Control

export(int) var speed

func setText(text):
	$Label.text = text
	yield(get_tree().create_timer(1.0), "timeout")
	queue_free()

func _process(delta):
	margin_top -= speed
