extends Control

export(Dictionary) var uiDict = {}
onready var button = get_node("Button")
var skill

func _ready():
	button.connect("pressed", self, "onButtonClicked")
	for i in uiDict:
		uiDict[i] = get_node(uiDict[i])
		uiDict[i].text = "" 
		
		

func setSkillUI(skill):
	self.skill = skill
	uiDict["name"].text = skill.skillName
	$Button.hint_tooltip = skill.desc
	if (not skill.isFinal):
		uiDict["xp"].text = "%s/%s" % [skill.currentXp, skill.xpNeed]
	if (skill.currentCd == 0):
		button.disabled = false
		uiDict["cd"].text = ""
	else:
		button.disabled = true
		if (skill.currentCd == 1):
			uiDict["cd"].text = "%s turn left" % skill.currentCd
		else:
			uiDict["cd"].text = "%s turns left" % skill.currentCd
		
func resetSkillUI():
	self.skill = null
	uiDict["name"].text = ""
	uiDict["xp"].text = ""
	uiDict["cd"].text = ""
	$Button.hint_tooltip = ""

func onButtonClicked():
	$Click.play()
	GameManager.onSkillClicked(skill)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
