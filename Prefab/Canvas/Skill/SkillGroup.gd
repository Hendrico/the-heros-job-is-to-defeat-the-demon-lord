extends Control


var skillCanvas = []
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _init():
	UiManager.uiDict["SkillGroup"] = self
# Called when the node enters the scene tree for the first time.
func _ready():
	skillCanvas.append_array(get_children())
	print(skillCanvas)

func setSkill(skills):
	for i in range(len(skills)):
		skillCanvas[i].setSkillUI(skills[i])
		
func resetSkill():
	for i in skillCanvas:
		i.resetSkillUI()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
