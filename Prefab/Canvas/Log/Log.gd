extends Control




# Called when the node enters the scene tree for the first time.
func _ready():
	UiManager.uiDict["log"] = self
	$Label.text = ""
	self.hide()

func reset():
	self.hide()
	$Label.text = ""
	
func addSkill():
	self.show()
	$Label.text = "%s -> " % GameManager.skill.skillName

func addDefender():
	$Label.text = "%s -> %s" % [GameManager.skill.skillName, GameManager.defenders.unitName]


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
