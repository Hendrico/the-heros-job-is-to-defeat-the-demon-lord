extends NinePatchRect


export(String) var sceneName

func _ready():
	GameManager.status["playerDeath"] = 0
	GameManager.status["enemyDeath"] = 0
	GameManager.players.clear()
	GameManager.enemies.clear()

func _on_Button_pressed():
	$Click.play()
	UiManager.uiDict["transitionScene"].changeScene(sceneName)
