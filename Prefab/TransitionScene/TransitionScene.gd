extends CanvasLayer

func _init():
	UiManager.uiDict["transitionScene"] = self

func _ready():
	$AnimationPlayer.play("FadeToNormal")

func changeScene(sceneName):
	$AnimationPlayer.play("FadeToBlack")
	yield($AnimationPlayer, "animation_finished")
	get_tree().change_scene(str("res://Prefab/Scene/" + sceneName + ".tscn"))
