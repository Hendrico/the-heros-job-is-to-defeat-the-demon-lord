extends Node

export(String) var skillName
export(String) var targetType
export(String, MULTILINE) var desc
export(int) var cd
var currentCd = 0
export(int) var xpNeed
var currentXp = 0
export(bool) var isFinal
export(Resource) var nextLevel
var buffs = []

func _ready():
	buffs = get_children()

func start():
	_ready()
	
func setValue(unit):
	for i in buffs:
		i.setValue(unit)

func reduceCooldown():
	currentCd -= 1
	if (currentCd < 0):
		currentCd = 0

func getUsed():
	currentCd = cd
	if (not isFinal):
		currentXp += 1
		if (currentXp >= xpNeed):
			get_parent().get_parent().levelUp(skillName, nextLevel)
			queue_free()
