extends Node

func activateSkill():
	print("%s used %s" % [GameManager.attackers.unitName, GameManager.skill.skillName])
	var isHit = false
	for buff in GameManager.skill.buffs:
		var targets = getTarget(buff)
		if (len(targets) == 0):
			continue
		for target in targets:
			if (is_instance_valid(target) and GameManager.isRun):
				isHit = true
				target.buffManager.addBuff(buff)
		if (isHit):
			yield(get_tree().create_timer(1.0), "timeout")
	if (GameManager.skill != null):
		GameManager.skill.getUsed()
	GameManager.attackers.endTurn()
	GameManager.state.endTurn()
	

func getTarget(buff):
	if (GameManager.attackers.is_in_group("Player")):
		if (buff.forAlly):
			return GameManager.players if buff.isAoe else [GameManager.attackers]
		else: 
			return GameManager.enemies if buff.isAoe else [GameManager.defenders]
	elif (GameManager.attackers.is_in_group("Enemy")):
		if (buff.forAlly):
			return GameManager.enemies if buff.isAoe else [GameManager.attackers]
		else:
			return GameManager.players if buff.isAoe else [GameManager.defenders]
